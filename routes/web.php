<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** Frontend Routing */
Route::prefix('/')->group(function () {
    Route::get('/', 'Site\HomeController@index')->name('home');
    Route::get('/summoners-wars-chronicles', 'Site\HomeController@swchronicles')->name('chronicles');
    Route::get('/about', 'Site\HomeController@about')->name('about');
    Route::get('/about/guild', 'Site\HomeController@guild')->name('about-the-guild');
    Route::get('/about/game', 'Site\HomeController@game')->name('about-the-game');

    /** Contact Form */
    Route::get('/contact', 'Admin\ContatoController@index')->name('contact');
	Route::post('/contact', 'Admin\ContatoController@send');

    Route::get('/message-listing',  ['middleware' => 'auth', 'uses' => 'Admin\ContatoController@list'])->name('message-listing');
});

/** Backend Routing */
Route::prefix('painel')->group(function () {
Route::get('/', 'Admin\HomeController@index')->name('painel');

/* Members Routing */
Route::resource('/members', 'Admin\MemberController');
});

Auth::routes();
