<?php

use Illuminate\Database\Seeder;
use App\Categoria;

class CategoriaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categoria::insert(['nome' => 'Dúvidas', 'descricao' => 'Tire suas dúvidas agora mesmo!']);
        Categoria::insert(['nome' => 'Sugestões', 'descricao' => 'Gostaria de sugerir algo?']);
        Categoria::insert(['nome' => 'Críticas', 'descricao' => 'Possui críticas construtivas? Conte-nos!']);
        Categoria::insert(['nome' => 'Recrutamento', 'descricao' => 'Gostaria de fazer parte da nossa família? Entre em contato!']);
    }
}
