<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('labirinto_id')->unsigned()->nullable();
            $table->bigInteger('gvg_id')->unsigned()->nullable();
            $table->bigInteger('siege_id')->unsigned()->nullable();
            $table->string('nome');
            $table->string('conta_principal');
            $table->string('conta_alt');
            $table->boolean('in_gvg');
            $table->boolean('in_siege');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::table('members', function($table) {
            $table->foreign('labirinto_id')->references('id')->on('labirintos')->onDelete('cascade');

            $table->foreign('gvg_id')->references('id')->on('gvgs')->onDelete('cascade');

            $table->foreign('siege_id')->references('id')->on('sieges')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
