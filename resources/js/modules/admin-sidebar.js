var mini = true;

function toggleSidebar() {
    if (mini) {
        console.log("opening sidebar");
        document.getElementById("adminSidebar").style.width = "250px";
        document.getElementById("mainContent").style.marginLeft = "250px";
        this.mini = false;
    } else {
        console.log("closing sidebar");
        document.getElementById("adminSidebar").style.width = "85px";
        document.getElementById("mainContent").style.marginLeft = "85px";
        this.mini = true;
    }
}
