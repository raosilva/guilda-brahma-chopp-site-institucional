@extends('layouts.frontend')
@section('content')

<!-- Page top section -->
<section class="page-top-section set-bg" data-setbg="/images/page-top-bg/1.jpg">
    <div class="page-info">
        <h2>Sobre</h2>
        <div class="site-breadcrumb">
            <a href="">Home</a> /
            <span>Sobre: O Jogo</span>
        </div>
    </div>
</section>
<!-- Page top end-->


<!-- Games section -->
<section class="games-single-page">
    <div class="container">
        <div class="game-single-preview">
            <img src="/images/sw750.png" alt="">
        </div>
        <div class="row">
            <div class="col-xl-9 col-lg-8 col-md-7 game-single-content">
                <div class="gs-meta">
                    <script>
                        var d = new Date();
                            var date = d. getDate();
                            var month = d. getMonth() + 1; // Since getMonth() returns month from 0-11 not 1-12.
                            var year = d. getFullYear();
                            var days = ["Domingo", "Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado"];
                            var months = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
                            var dateStr = days[date] + ", " + date + " de " + months[month] + " de " + year;
                            document. write(dateStr);
                    </script>
                    / em <a href="">Summoners Wars News</a>
                </div>
                <h2 class="gs-title">Summoners Wars: Sky Arena</h2>
                <h4>Overview</h4>
                <p><strong>Summoners War: Sky Arena</strong> é um jogo de RPG com combates em turnos, com versões para
                    Android e iOS. Sua missão neste game é evocar criaturas e treiná-las em batalha, com o objetivo de
                    reencontrar os cristais de mana. Summoners War: Sky Arena mistura vários estilos com elementos de
                    RPG e estratégia no estilo “Clash of Clans”. <br><br>

                    Em Summoners War: Sky Arena, são mais de 400 monstros para colecionar e evoluir. Ao utilizar os
                    monstros em combates, o jogador ganha pontos de experiência, energia e cristais. Estes últimos podem
                    ser utilizados para evocar mais monstros. Pode-se utilizar vários tipos de cristais para evocar
                    monstros. <br><br>

                    Cada monstro evocado possui sua própria árvore de evolução e o jogador pode coletar runas entre as
                    batalhas para evoluir os monstros mais rapidamente. As batalhas em Summoners War: Sky Arena
                    acontecem de forma sequencial, por isso prepare uma equipe de monstros para cara fase.</p>

                <h4>Batalhas online incríveis</h4>

                <p>Um dos destaques de Summoners War: Sky Arena é que o jogo une de forma curiosa o colecionismo de
                    monstro no melhor estilo Pokémon, com batalhas de defesa de vilas, como em Clash of Clans. Além de
                    evocar e colecionar monstros, o jogador também deverá construir e fortificar sua ilha no céu.
                    <br><br>

                    Isso é necessário porque depois de alguns níveis será aberto o modo de combate contra outros
                    jogadores. Nesse modo você poderá atacar as ilhas deles, assim como eles poderão atacar a sua.
                    <br><br>

                    Summoners War: Sky Arena é um rico jogo de estratégia com gráficos incríveis e jogabilidade
                    simplificada. O jogo recompensa o jogador que joga todo dia com muitos cristais de evocação e outros
                    prêmios. <br><br>

                    O game exige conexão constante com a Internet. Após o download inicial é necessário baixar os dados
                    do jogo, reserve um bom espaço de armazenamento, pois são mais de 370 Mbs de download. Baixe
                    Summoners War: Sky Arena grátis agora mesmo!</p>

                <h2>Opinião do Escritor</h2>

                <p><span style="font-size: 1.25rem;">Summoners War: Sky Arena é um jogo que chama atenção pela proposta
                        e jogabilidade. Misturando estilos diferentes, o game se propõe a ser um RPG, mas com muitos
                        elementos de estratégia. O jogo apresenta gráficos em 3D muito bonitos e coloridos, e música
                        cativante, no clima de uma aventura épica.</span> <br><br>

                    A jogabilidade é um dos pontos fortes do jogo. Simples e direta, a ação acontece com poucos toques
                    na tela. Cada monstro começa o jogo com direito a dois comandos, um ataque normal e outra habilidade
                    que pode ser ofensiva ou de cura. As possibilidades de evolução são muitas e cada monstro ainda pode
                    ser equipado com runas, o que atribui um elemento da natureza ao monstro.<br><br>

                    Outro ponto alto do game é a necessidade de construir e evoluir uma pequena vila. Nada muito
                    elaborado, pois o foco do jogo são os combates. Neste aspecto, Summoners War: Sky Arena brilha com
                    uma ampla variedade de possibilidades graças às evoluções dos monstros.<br><br>

                    Porém, Summoners War: Sky Arena também deixa a desejar em alguns aspectos. A história é muito rasa e
                    com pouco desenvolvimento dos personagens. Os limitadores de energia fazem o jogador ter direito a
                    apenas algumas partidas diárias e o jogo exige muita conexão com a Internet, necessitando de
                    downloads constantes.

                    <h4>
                        Prós
                    </h4>

                    <ul style="list-style: disc;margin-left:3rem;">
                        <li style="font-size: 15px;color: #68647d;line-height: 1.6;font-weight: 500;">Ótimos gráficos
                        </li>
                        <li style="font-size: 15px;color: #68647d;line-height: 1.6;font-weight: 500;">Jogabilidade
                            viciante</li>
                        <li style="font-size: 15px;color: #68647d;line-height: 1.6;font-weight: 500;">Combates contra
                            outros jogadores</li>
                    </ul>
                    <br>
                    <h4>
                        Contras
                    </h4>

                    <ul style="list-style: disc;margin-left:3rem;">
                        <li style="font-size: 15px;color: #68647d;line-height: 1.6;font-weight: 500;">Exige conexão com
                            a Internet</li>
                        <li style="font-size: 15px;color: #68647d;line-height: 1.6;font-weight: 500;">Downloads
                            constantes</li>
                    </ul>
                </p>


                <div class="geme-social-share pt-5 d-flex">
                    <p>Compartilhe:</p>
                    <a href="#"><i class="fa fa-pinterest"></i></a>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-instagram"></i></a>
                    <a href="#"><i class="fa fa-youtube"></i></a>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-5 sidebar game-page-sideber">
                <div id="stickySidebar">
                    <div class="widget-item">
                        <div class="rating-widget">
                            <h4 class="widget-title">Ratings</h4>
                            <ul>
                                <li>P2W<span>4.5/5</span></li>
                                <li>Gráficos<span>4.5/5</span></li>
                                <li>Level<span>3.5/5</span></li>
                                <li>História<span>3.0/5</span></li>
                                <li>Jogabilidade<span>4.5/5</span></li>
                                <li>Dificuldade<span>4.0/5</span></li>
                            </ul>
                            <div class="rating">
                                <h5><i>Rating</i><span>4.0</span> / 5</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Games end-->

<section class="game-author-section">
    <div class="container">
        <div class="game-author-pic set-bg" data-setbg="/images/techtudo.jpg"></div>
        <div class="game-author-info">
            <h4>Fonte: <a href="https://www.techtudo.com.br/tudo-sobre/summoners-war-sky-arena.html" target="_blank">Techtudo</a></h4>
            <p>TechTudo é um site da Globo.com que tem com assunto principal a tecnologia, tratando de assuntos como jogos, celulares, softwares, eletrônicos, TV e internet. É considerado um dos melhores sites de tecnologia feito em português.</p>
        </div>
    </div>
</section>

@endsection