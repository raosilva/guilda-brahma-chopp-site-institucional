@extends('layouts.frontend')
@section('content')

<!-- Hero section -->
<section class="hero-section overflow-hidden">
    <div class="hero-slider owl-carousel">
        <div class="hero-item set-bg d-flex align-items-center justify-content-center text-center"
            data-setbg="./images/hero-1.jpg">
            <div class="container">
                <h2>Game on!</h2>
                <p>Está sozinho no jogo e não tem com quem compartilhar suas aventuras?<br>
                    Gostaria de aprender muito mais e chegar mais longe no game!?<br>
                    Junte-se a nós!!!</p>
                <a href="{{route('about-the-guild')}}" class="site-btn">Leia Mais... <img
                        src="./images/icons/double-arrow.png" alt="#" /></a>
            </div>
        </div>
        <div class="hero-item set-bg d-flex align-items-center justify-content-center text-center"
            data-setbg="./images/sw-bg-1.png">
            <div class="container">
                <h2>Game on!</h2>
                <p><strong>Summoners War: Sky Arena</strong> is a mobile turn-based strategy massively multiplayer
                    online game created by <br> South Korean game developer, Com2uS. <br>
                    Summoners War has performed successfully, with 150,000,000 downloads worldwide.</p>
                <a href="{{route('about-the-game')}}" class="site-btn">Leia Mais... <img
                        src="./images/icons/double-arrow.png" alt="#" /></a>
            </div>
        </div>
    </div>
</section>
<!-- Hero section end-->


{{-- <!-- Intro section -->
<section class="intro-section">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="intro-text-box text-box text-white">
                    <div class="top-meta">11.11.18 / in <a href="">Games</a></div>
                    <h3>The best online game is out now!</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida....</p>
                    <a href="#" class="read-more">Read More <img src="./images/icons/double-arrow-red.png"
                            alt="#" /></a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="intro-text-box text-box text-white">
                    <div class="top-meta">11.11.18 / in <a href="">Playstation</a></div>
                    <h3>Top 5 best games in november</h3>
                    <p>Ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                        et dolore magna aliqua. Quis ipsum labore suspendisse ultrices gravida....</p>
                    <a href="#" class="read-more">Read More <img src="./images/icons/double-arrow-red.png"
                            alt="#" /></a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="intro-text-box text-box text-white">
                    <div class="top-meta">11.11.18 / in <a href="">Reviews</a></div>
                    <h3>Get this game at a promo price</h3>
                    <p>Sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Quis ipsum suspendisse ultrices gravida ncididunt ut labore ....</p>
                    <a href="#" class="read-more">Read More <img src="./images/icons/double-arrow-red.png"
                            alt="#" /></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Intro section end -->


<!-- Blog section -->
<section class="blog-section spad">
    <div class="container">
        <div class="row">
            <div class="col-xl-9 col-lg-8 col-md-7">
                <div class="section-title text-white">
                    <h2>Latest News</h2>
                </div>
                <ul class="blog-filter">
                    <li><a href="#">Racing</a></li>
                    <li><a href="#">Shooters</a></li>
                    <li><a href="#">Strategy</a></li>
                    <li><a href="#">Online</a></li>
                </ul>
                <!-- Blog item -->
                <div class="blog-item">
                    <div class="blog-thumb">
                        <img src="./images/blog/1.jpg" alt="">
                    </div>
                    <div class="blog-text text-box text-white">
                        <div class="top-meta">11.11.18 / in <a href="">Games</a></div>
                        <h3>The best online game is out now!</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eius-mod tempor
                            incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.
                            Lorem ipsum dolor sit amet, consecte-tur adipiscing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliqua.....</p>
                        <a href="#" class="read-more">Read More <img src="./images/icons/double-arrow-red.png" alt="#" /></a>
                    </div>
                </div>
                <!-- Blog item -->
                <div class="blog-item">
                    <div class="blog-thumb">
                        <img src="./images/blog/2.jpg" alt="">
                    </div>
                    <div class="blog-text text-box text-white">
                        <div class="top-meta">11.11.18 / in <a href="">Games</a></div>
                        <h3>The best online game is out now!</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eius-mod tempor
                            incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.
                            Lorem ipsum dolor sit amet, consecte-tur adipiscing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliqua.....</p>
                        <a href="#" class="read-more">Read More <img src="./images/icons/double-arrow-red.png"
                                alt="#" /></a>
                    </div>
                </div>
                <!-- Blog item -->
                <div class="blog-item">
                    <div class="blog-thumb">
                        <img src="./images/blog/3.jpg" alt="">
                    </div>
                    <div class="blog-text text-box text-white">
                        <div class="top-meta">11.11.18 / in <a href="">Games</a></div>
                        <h3>The best online game is out now!</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eius-mod tempor
                            incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.
                            Lorem ipsum dolor sit amet, consecte-tur adipiscing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliqua.....</p>
                        <a href="#" class="read-more">Read More <img src="./images/icons/double-arrow-red.png"
                                alt="#" /></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-5 sidebar">
                <div id="stickySidebar">
                    <div class="widget-item">
                        <h4 class="widget-title">Trending</h4>
                        <div class="trending-widget">
                            <div class="tw-item">
                                <div class="tw-thumb">
                                    <img src="./images/blog-widget/1.jpg" alt="#">
                                </div>
                                <div class="tw-text">
                                    <div class="tw-meta">11.11.18 / in <a href="">Games</a></div>
                                    <h5>The best online game is out now!</h5>
                                </div>
                            </div>
                            <div class="tw-item">
                                <div class="tw-thumb">
                                    <img src="./images/blog-widget/2.jpg" alt="#">
                                </div>
                                <div class="tw-text">
                                    <div class="tw-meta">11.11.18 / in <a href="">Games</a></div>
                                    <h5>The best online game is out now!</h5>
                                </div>
                            </div>
                            <div class="tw-item">
                                <div class="tw-thumb">
                                    <img src="./images/blog-widget/3.jpg" alt="#">
                                </div>
                                <div class="tw-text">
                                    <div class="tw-meta">11.11.18 / in <a href="">Games</a></div>
                                    <h5>The best online game is out now!</h5>
                                </div>
                            </div>
                            <div class="tw-item">
                                <div class="tw-thumb">
                                    <img src="./images/blog-widget/4.jpg" alt="#">
                                </div>
                                <div class="tw-text">
                                    <div class="tw-meta">11.11.18 / in <a href="">Games</a></div>
                                    <h5>The best online game is out now!</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="widget-item">
                        <div class="categories-widget">
                            <h4 class="widget-title">Categories</h4>
                            <ul>
                                <li><a href="">Games</a></li>
                                <li><a href="">Gaming Tips & Tricks</a></li>
                                <li><a href="">Online Games</a></li>
                                <li><a href="">Team Games</a></li>
                                <li><a href="">Community</a></li>
                                <li><a href="">Uncategorized</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="widget-item">
                        <a href="#" class="add">
                            <img src="./images/add.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Blog section end --> --}}


<!-- Intro section -->
<section class="intro-video-section set-bg d-flex align-items-end " data-setbg="./images/summonerversary.png">
    <a href="https://www.youtube.com/watch?v=GyNxFTWGOrY" class="video-play-btn video-popup"><img
            src="./images/icons/solid-right-arrow.png" alt="#"></a>
    <div class="container">
        <div class="video-text">
            <h2>Vídeo promocional</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
        </div>
    </div>
</section>
<!-- Intro section end -->


<!-- Featured section -->
<section class="featured-section">
    <div class="featured-bg set-bg" data-setbg="./images/sw-2.jpg"> </div>
    <div class="featured-box">
        <div class="text-box">
            <div class="top-meta"> 26.01.2020 / em <a href="">Summoners Wars</a></div>
            <h3>Summoners War: Chronicles</h3>
            <p>A Com2us anunciou em janeiro seu novo game em desenvolvimento, Summoners War: Chronicles, um novo MMORPG baseado em Summoners War: Sky Arena, disponível gratuitamente para Android e iOS.</p>
        <a href="{{route('chronicles')}}" class="read-more">Leia Mais... <img src="./images/icons/double-arrow.png" alt="#" /></a>
        </div>
    </div>
</section>
<!-- Featured section end-->

{{-- <!-- Newsletter section -->
<section class="newsletter-section">
    <div class="container">
        <h2>Subscribe to our newsletter</h2>
        <form class="newsletter-form">
            <input type="text" placeholder="ENTER YOUR E-MAIL">
            <button class="site-btn">subscribe <img src="./images/icons/double-arrow-red.png" alt="#" /></button>
        </form>
    </div>
</section>
<!-- Newsletter section end --> --}}

@endsection