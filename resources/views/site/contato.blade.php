@extends('layouts.frontend')
@section('content')

<!-- Page top section -->
<section class="page-top-section set-bg" data-setbg="/images/page-top-bg/1.jpg">
    <div class="page-info">
        <h2>Dúvidas, críticas ou sugestões</h2>
        <div class="site-breadcrumb">
            <a href="">Home</a> /
            <span>Contate-nos</span>
        </div>
    </div>
</section>
<!-- Page top end-->

<!-- Contact page -->
<section class="contact-page">
    <div class="container">
        {{-- <div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14376.077865872314!2d-73.879277264103!3d40.757667781624285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1546528920522" style="border:0" allowfullscreen></iframe></div> --}}
        <div class="row">
            <div class="col-lg-7 order-2 order-lg-1">

                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <form class="contact-form" action="/contact" method="POST">
                    @csrf

                    <label for="nome">Categoria</label>
            <select id="categoria_id" name="categoria_id" class="form-control mb-4">
                <option value="null">Selecione a categoria</option>
 
                @foreach (App\Categoria::all() as $categoria)
                    <option value="{{ $categoria->id }}">{{ $categoria->nome }}</option>
                @endforeach
 
            </select>
                    <input type="text" name="nome" placeholder="Digite seu nome">
                    <input type="email" name="email" placeholder="Digite o seu e-mail">
                    <input type="text" name="assunto" placeholder="Assunto">
                    <textarea name="mensagem" placeholder="Digite aqui o que deseja"></textarea>
                    <button class="site-btn">Enviar mensagem<img src="/images/icons/double-arrow.png"
                            alt="#" /></button>
                </form>
            </div>

            <div class="col-lg-5 order-1 order-lg-2 contact-text text-white">
                <h3>E aí!? Tudo beleza?</h3>
                <p>Caso tenha alguma dúvida sobre qualquer área do site ou seu conteúdo, bem como críticas construtivas
                    ou sugestões, fique a vontade de nos enviar pelo formulário ao lado, via whatsapp ou no e-mail
                    abaixo que faremos sempre o possível para podermos melhorar para você.</p>
                <div class="cont-info">
                    <div class="ci-icon"><img src="/images/icons/phone.png" alt=""></div>
                    <div class="ci-text">
                        <a href="tel:+5511947485240">+55 (11) 9 4748 5240</a>
                        <img src="./images/icons/double-arrow.png" alt="#" class="ml-3 mr-3" />
                        SmaxxioNVD (Site Admin)
                    </div>
                </div>
                <div class="cont-info">
                    <div class="ci-icon"><img src="/images/icons/mail.png" alt=""></div>
                    <div class="ci-text"><a href="mailto:guildabrahmachopp@gmail.com?subject=Contato%-%Guilda%Brahma%Chopp">guildabrahmachopp@gmail.com</a></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Contact page end-->

@endsection