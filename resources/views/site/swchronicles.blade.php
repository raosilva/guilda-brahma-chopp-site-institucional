@extends('layouts.frontend')

@section('title', 'News :: Summoners Wars: Chronicles')

@section('content')

<!-- Page top section -->
<section class="page-top-section set-bg" data-setbg="/images/page-top-bg/1.jpg">
    <div class="page-info">
        <h2>Summoners Wars: Chronicles</h2>
        <div class="site-breadcrumb">
            <a href="">Home</a> /
            <span>Sobre: O Jogo</span>
        </div>
    </div>
</section>
<!-- Page top end-->


<!-- Games section -->
<section class="games-single-page">
    <div class="container">
        <div class="game-single-preview">
            <img src="/images/sw750.png" alt="">
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 game-single-content">
                <div class="gs-meta">
                    <script>
                        var d = new Date();
                            var date = d. getDate();
                            var month = d. getMonth() + 1; // Since getMonth() returns month from 0-11 not 1-12.
                            var year = d. getFullYear();
                            var days = ["Domingo", "Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado"];
                            var months = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
                            var dateStr = days[date] + ", " + date + " de " + months[month] + " de " + year;
                            document. write(dateStr);
                    </script>
                    / em <a href="">Summoners Wars News</a>
                </div>
                <h2 class="gs-title">Summoners Wars: Chronicles</h2>
                <h4>Overview</h4>
                <p>A <span style="color: #b01ba5;font-weight:bold;">Com2us</span> anunciou em 26/01/2020 seu novo game
                    em desenvolvimento, Summoners War: Chronicles, um novo MMORPG baseado em <span
                        style="color: #b01ba5;font-weight:bold;">Summoners War: Sky Arena</span>, disponível
                    gratuitamente para Android e iOS. <br><br>
                    Ainda sem muitas informações reveladas, <span style="color: #b01ba5;font-weight:bold;">Summoners
                        War: Chronicles</span> é um MMORPG situado 70 anos antes dos acontecimentos de Summoners War:
                    Sky Arena, game de estratégia baseado em turnos lançado em 2014 no Android e iOS. <br><br>

                    No game, os jogadores poderão escolher dentre três personagens com habilidades únicas para
                    participar de batalhas em tempo real com até outros três outros monstros invocados por personagem.
                    Os personagens poderão compartilhar os monstros e recursos, permitindo aos jogadores fazer escolhas
                    estratégicas para uma jogabilidade diversificada. <br><br>

                    <strong>Confira as imagens do game baixo:</strong> <br><br>

                    <div class="col-xl-8 col-lg-7 col-md-6 mx-auto">
                        <img src="/images/swchronicle2.png" alt="" width="768" height="411" class="mx-auto -flex justify-content-center flex-wrap"><br><br>
                        <img src="/images/swchronicle.png" alt="" class="mx-auto -flex justify-content-center flex-wrap">
                    </div>

                </p>



                <div class="geme-social-share pt-5 d-flex">
                    <p>Compartilhe:</p>
                    <a href="#"><i class="fa fa-pinterest"></i></a>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-instagram"></i></a>
                    <a href="#"><i class="fa fa-youtube"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Games end-->

<section class="game-author-section">
    <div class="container">
        <div class="game-author-pic set-bg" data-setbg="/images/techtudo.jpg"></div>
        <div class="game-author-info">
            <h4>Fonte: <a href="https://www.techtudo.com.br/tudo-sobre/summoners-war-sky-arena.html"
                    target="_blank">Techtudo</a></h4>
            <p>TechTudo é um site da Globo.com que tem com assunto principal a tecnologia, tratando de assuntos como
                jogos, celulares, softwares, eletrônicos, TV e internet. É considerado um dos melhores sites de
                tecnologia feito em português.</p>
        </div>
    </div>
</section>

@endsection