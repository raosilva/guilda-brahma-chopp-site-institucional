@extends('layouts.frontend')
@section('content')

<!-- Page top section -->
<section class="page-top-section set-bg" data-setbg="/images/page-top-bg/1.jpg">
    <div class="page-info">
        <h2>Sobre</h2>
        <div class="site-breadcrumb">
            <a href="">Home</a>  /
            <span>Sobre: A Guilda</span>
        </div>
    </div>
</section>
<!-- Page top end-->


<!-- Games section -->
<section class="games-single-page">
    <div class="container">
        <div class="game-single-preview">
            <img src="/images/brahma-new.png" alt="">
        </div>
        <div class="row">
            <div class="col-xl-9 col-lg-8 col-md-7 game-single-content">
                <div class="gs-meta">
                    <script>
                        var d = new Date();
                        var date = d. getDate();
                        var month = d. getMonth() + 1; // Since getMonth() returns month from 0-11 not 1-12.
                        var year = d. getFullYear();
                        var days = ["Domingo", "Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado"];
                        var months = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
                        var dateStr = days[date] + ", " + date + " de " + months[month] + " de " + year;
                        document. write(dateStr);
                        </script>
                    /  no <a href="">Salão da Guilda</a>
                </div>
                <h2 class="gs-title">Bem-vindo ao Salão da Guilda</h2>
                <h4>Olá, Evocadores!</h4>
                <p>Sejam bem-vindos à GUILDA BRAHMA CHOPP, somos uma guilda <strong>farm</strong> com foco em ajudar vocês a evoluirem no SW. <br>
                Temos uma comunidade bem participativa e todos nosso membros experientes estão aí para ajudar no que precisarem com dicas. <br> 
                Também, em nosso site você encontrará diversos tutoriais que te ajudarão a progredir no jogo e a sair daquela fase que você empacou ou facilitar para que passe de primeira qualquer parte pve do jogo, e qualquer dúvida, estamos sempre à disposição.</p>

                <a href="{{route('recruitment')}}" class="site-btn">Vem com a gente! <img
                    src="/images/icons/double-arrow.png" alt="#" /></a>

                <h2 style="margin-top: 3rem;">Ranking</h2>

                <h4>Guild Wars</h4>
                <p><i class="fas fa-star" style="color: #EEE"></i>
                    <i class="fas fa-star" style="color: #EEE"></i>
                    <i class="fas fa-star" style="color: #EEE"></i></p>

                <h4>Siege</h4>
                <p><i class="fas fa-star" style="color: #EEE"></i>
                    <i class="fas fa-star" style="color: #EEE"></i>
                    <i class="fas fa-star" style="color: #EEE"></i></p>

                <h4>Labirinto</h4>
                <p><strong><span style="font-size: 1.5rem; color: #f57904">A+ / S</span></strong></p>

                <div class="geme-social-share pt-5 d-flex">
                    <p>Compartilhe:</p>
                    <a href="#"><i class="fa fa-pinterest"></i></a>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-instagram"></i></a>
                    <a href="#"><i class="fa fa-youtube"></i></a>
                </div>

            </div>
            <div class="col-xl-3 col-lg-4 col-md-5 sidebar game-page-sideber">
                <div id="stickySidebar">
                    <div class="widget-item">
                        <h4 class="widget-title">Alta Cúpula</h4>
                        <div class="latest-comments">
                            <div class="lc-item">
                                <img src="/images/roseli.jpeg" class="lc-avatar" alt="#">
                                <div class="tw-text"><a href="">Lider</a><br>
                                    <span><i class="far fa-heart"></i>
                                        <i class="far fa-heart"></i>
                                    Roseli
                                    <i class="far fa-heart"></i>
                                    <i class="far fa-heart"></i></span></div>
                            </div>
                            <div class="lc-item">
                                <img src="/images/anderson15.jpeg" class="lc-avatar" alt="#">
                                <div class="tw-text"><a href="">Vice-Lider</a><br>
                                    <span>anderson15</span></div>
                            </div>
                            <div class="lc-item">
                                <img src="/images/leehgenji.jpeg" class="lc-avatar" alt="#">
                                <div class="tw-text"><a href="">Vice-Lider</a><br>
                                    <span>leehgenji</span></div>
                            </div>
                            <div class="lc-item">
                                <img src="/images/smaxxionvd.jpg" class="lc-avatar" alt="#">
                                <div class="tw-text"><a href="">Site Admin</a><br>
                                    <span>SmaxxioNVD</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Games end-->

<section class="game-author-section">
    <div class="container">
        <div class="game-author-pic set-bg" data-setbg="/images/thumb1.jpg"></div>
        <div class="game-author-info">
            <h4>Escrito por: SmaxxioNVD</h4>
            <p>Desenvolvedor do website e membro ativo da guilda.</p>
        </div>
    </div>
</section>

@endsection