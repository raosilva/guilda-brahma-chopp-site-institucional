@extends('layouts.frontend')
@section('content')

<!-- Page top section -->
<section class="page-top-section set-bg" data-setbg="./images/top-bg.png">
    <div class="page-info">
        <h2>Sobre</h2>
        <div class="site-breadcrumb">
            <a href="">Home</a>  /
            <span>Sobre</span>
        </div>
    </div>
</section>
<!-- Page top end-->


<!-- Games section -->
<section class="games-single-page">
    <div class="container">
        <div class="game-single-preview">
            <a href="{{route('about-the-guild')}}"><img src="./images/brahma-new.png" alt=""></a>
        </div>

        <div class="game-single-preview">
        <a href="{{route('about-the-game')}}"><img src="./images/promo-bg2.jpg" alt=""></a>
        </div>
    </div>
</section>
        
@endsection