@extends('layouts.backend')

@section('title', 'Cadastrar Membro')

@section('content_header')
<h1>Novo Membro</h1>
@endsection

@section('mainArea')

@if($errors->any())
<div class="alert alert-danger">
    <h5> <i class="icon fa s fa-ban"></i> Alerta! </h5>
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="card">
    <div class="card-body">
        <form action="{{ route('members.store') }}" method="post" class="form-horizontal">
            @csrf
            <div class="form-group row">
                <label for="name" class="col-form-label col-sm-2">Nome :</label>
                <div class="col-sm-10">
                    <input type="text" name="nome" id="nome" class="form-control @error('nome') is-invalid @enderror"
                        value="{{ old('nome') }}">
                </div>
            </div>

            <div class="form-group row">
                <label for="conta_principal" class="col-form-label col-sm-2">Conta #1:</label>
                <div class="col-sm-10">
                    <input type="text" name="conta_principal" id="conta_principal"
                        class="form-control @error('conta_principal') is-invalid @enderror" value="{{ old('conta_principal') }}">
                </div>
            </div>

            <div class="form-group row">
                <label for="conta_alt" class="col-form-label col-sm-2">Conta #2:</label>
                <div class="col-sm-10">
                    <input type="text" name="conta_alt" id="conta_alt"
                        class="form-control @error('conta_alt') is-invalid @enderror">
                </div>
            </div>

            <div class="form-group row">
                <label for="in_gvg" class="col-form-label col-sm-2">Está na GvG?</label>
                <div class="col-sm-10">
                    <input type="radio" name="password_confirmation" id="password_confirmation"
                        class="form-control @error('password') is-invalid @enderror">
                </div>
            </div>

            <div class="form-group row">
                <label for="addNewUser" class="col-form-label col-sm-2"></label>
                <div class="col-sm-10">
                    <input type="submit" name="addNewUser" id="addNewUser" value="Cadastrar" class="btn btn-success">
                </div>
            </div>
        </form>
    </div>
</div>

@endsection