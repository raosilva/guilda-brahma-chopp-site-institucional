@extends('layouts.backend')

@section('title', 'Membros')

@section('content_header')
<h1>Meus Usuários
    <a href="{{ route('members.create') }}" class="btn btn-sm btn-success ml-3 text-uppercase">Adicionar Usuário</a>
</h1>
@endsection

@section('mainArea')

<div class="card">
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>Conta #1</th>
                    <th>Conta #2</th>
                    <th>Está na GvG</th>
                    <th>Está na Siege?</th>
                    <th>Ações</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($members as $member)
                <tr>
                    <th>{{ $member->id }}</th>
                    <th>{{ $member->nome }}</th>
                    <th>{{ $member->conta_principal }}</th>
                    <th>{{ $member->conta_alt }}</th>
                    <th>{{ $member->in_gvg }}</th>
                    <th>{{ $member->in_siege }}</th>
                    <th>
                        <a href="{{ route('members.edit', ['member' => $member->id]) }}"
                            class="btn btn-sm btn-info">Editar</a>

                        @if ($loggedId !== intval($member->id))
                        <form action="{{ route('members.destroy', ['member' => $member->id]) }}" method="POST"
                            class="d-inline" onsubmit="return confirm('Tem certeza que deseja excluir este usuário?')">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="Excluir" class="btn btn-sm btn-danger">
                            @endif
                        </form>
                    </th>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

{{$members->links()}}

@endsection