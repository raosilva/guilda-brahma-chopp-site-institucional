@extends('adminlte::page')

@section('title', 'Adicionar Usuário')

@section('content_header')
<h1>Novo Usuário</h1>
@endsection

@section('content')

@if($errors->any())
<div class="alert alert-danger">
    <h5> <i class="icon fa s fa-ban"></i> Alerta! </h5>
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="card">
    <div class="card-body">
        <form action="{{ route('users.store') }}" method="post" class="form-horizontal">
            @csrf
            <div class="form-group row">
                <label for="name" class="col-form-label col-sm-2">Nome :</label>
                <div class="col-sm-10">
                    <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror"
                        value="{{ old('name') }}">
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-form-label col-sm-2">E-mail:</label>
                <div class="col-sm-10">
                    <input type="email" name="email" id="email"
                        class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-form-label col-sm-2">Senha:</label>
                <div class="col-sm-10">
                    <input type="password" name="password" id="password"
                        class="form-control @error('password') is-invalid @enderror">
                </div>
            </div>

            <div class="form-group row">
                <label for="password_confirmation" class="col-form-label col-sm-2">Confirme a Senha:</label>
                <div class="col-sm-10">
                    <input type="password" name="password_confirmation" id="password_confirmation"
                        class="form-control @error('password') is-invalid @enderror">
                </div>
            </div>

            <div class="form-group row">
                <label for="addNewUser" class="col-form-label col-sm-2"></label>
                <div class="col-sm-10">
                    <input type="submit" name="addNewUser" id="addNewUser" value="Cadastrar" class="btn btn-success">
                </div>
            </div>
        </form>
    </div>
</div>

@endsection