@extends('layouts.backend')

@section('mainArea')

<div id="mainContent">
   <div class="card">
      <div class="card-header">
         Painel de Controle
      </div>
      <div class="card-body">
         @if (session('status'))
         <div class="alert alert-success" role="alert">
            {{ session('status') }}
         </div>
         @endif

         Você está logado!
      </div>
   </div>
</div>

@endsection