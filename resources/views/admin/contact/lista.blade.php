@extends('layouts.backend')

@section('mainArea')

<div id="mainContent">
  <div class="card">
    <div class="card-header">
      <h2>
        Lista de Mensagens
      </h2>
    </div>
    <div class="card-body">
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th>#</th>
            <th>Categoria</th>
            <th>Nome</th>
            <th>Email</th>
            <th>Assunto</th>
            <th>Mensagem</th>
          </tr>
        </thead>
        <tbody>
          @foreach($contatos as $row)
          <tr>
            <th scope="row">{{ $row->id }}</th>
            <td>{{ $row->categoria->nome}}</td>
            <td>{{ $row->nome }}</td>
            <td>{{ $row->email }}</td>
            <td>{{ $row->assunto }}</td>
            <td>{{ $row->mensagem }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>

  {{$members->links()}}
</div>

@endsection