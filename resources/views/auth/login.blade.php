<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }} - Summoners Wars</title>
  <meta name="keywords"
    content="Gaming Login Form Widget Tab Form,Login Forms,Sign up Forms,Registration Forms,News letter Forms,Elements" />

  <script type="application/x-javascript">
    addEventListener("load", function () {
      setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
      window.scrollTo(0, 1);
    }
  </script>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


  <!-- Scripts -->
  <script src="{{ asset('js/scripts.js') }}" defer></script>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <!-- Styles -->
  <link href="{{ asset('css/login.css') }}" rel="stylesheet">
</head>

<body>
  <!-- Right Side Of Navbar -->
  <ul class="navbar-nav ml-auto">
    <!-- Authentication Links -->
    @auth
      <li class="nav-item dropdown">
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false" v-pre>
          {{ Auth::user()->name }} <span class="caret"></span>
        </a>

        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </div>
      </li>
    @endauth

    <div class="padding-all">
      <div class="header">
          <h1>{{ config('app.name', 'Laravel') }}</h1>
        <h1>Formulário de Login</h1>
      </div>

      <div class="design-w3l">
        <div class="mail-form-agile">
          <form method="POST" action="{{ route('login') }}">
            <input id="email" type="email" class="@error('email') is-invalid @enderror" name="email"
              value="{{ old('email') }}" placeholder="Digite seu e-mail..." required
              autocomplete="email" autofocus>

            @error('email')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror

            <input id="password" type="password" class="padding @error('password') is-invalid @enderror" name="password"
              placeholder="Digite sua senha..." required autocomplete="current-password">

            @error('password')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror

            <div class="form-group row">
                <div class="col-md-12 offset-md-4">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label pt-1" for="remember" style="color: #9c0909;">
                            {{ __('Lembrar-me') }}
                        </label>
                    </div>
                </div>
            </div>

            <input type="submit" value="{{ __('Login') }}" />
                    @if (Route::has('password.request'))
                        <a class="btn-default ml-3" href="{{ route('password.request') }}">
                            {{ __('Esqueceu a senha?') }}
                        </a>
                    @endif
            </div>
          </form>
        </div>
        <div class="clear"></div>
      </div>

      <div class="footer">
        <p>
          Copyright &copy;
          <script>
            document.write(new Date().getFullYear());
          </script>
          All rights reserved | | Design by
          <a href="#"> SmaxxioNVD </a>
        </p>
      </div>
    </div>
</body>

</html>