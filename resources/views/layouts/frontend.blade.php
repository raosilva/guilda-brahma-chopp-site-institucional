<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Site de Recrutamento e Gerenciamento da Guilda Brahma Chopp - Summoners Wars">
    <meta name="keywords" content="summoners, wars, summoners wars, game, gaming, guild, guilda brahma chopp">

    <!-- CSRF Token -->
    <meta name=" csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Guilda Brahma Chopp') }} - Summoners Wars</title>

    <!-- Favicon -->
    <link href="/public/images/favicon.ico" rel="shortcut icon" />

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <script src="https://kit.fontawesome.com/e943c11967.js" crossorigin="anonymous"></script>

    <!-- Stylesheets -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slicknav.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/magnific-popup.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">

    <!-- Main Stylesheets -->
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

    <!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>

    <!-- Page Preloader -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header section -->
    <header class="header-section">
        <div class="header-warp">
            <div class="header-social d-flex justify-content-end">
                <p>Follow us:</p>
                <a href="#"><i class="fa fa-pinterest"></i></a>
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-instagram"></i></a>
                <a href="#"><i class="fa fa-youtube"></i></a>
            </div>
            <div class="header-bar-warp d-flex">
                <!-- site logo -->
                <a href="{{ route('home') }}" class="site-logo">
                    <img src="./images/logo.png" alt="">
                </a>
                <nav class="top-nav-area w-100">
                    <div class="user-panel">
                        @guest
                        <a href="{{ route('login') }}">Login</a> /
                        @if (Route::has('register'))
                        <a href="{{ route('register') }}">Register</a>
                        @endif
                        @else
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                        @endguest
                    </div>
                    <!-- Menu -->
                    <ul class="main-menu primary-menu">
                        <li class="{{ (strpos(Route::currentRouteName(), 'home') === 0) ? 'active' : '' }}">
                            <a href="{{ route('home') }}">Home</a></li>
                        <li class="{{ request()->is('about*') ? 'active' : '' }}"><a href="{{route('about')}}">Sobre</a>
                            <ul class="sub-menu">
                                <li><a href="{{route('about-the-guild')}}">A Guilda</a></li>
                                <li><a href="{{route('about-the-game')}}">O Jogo</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Tutorials</a>
                            <ul class="sub-menu">
                                <li><a href="#">Evoluindo Com Eficiência</a></li>
                                <li><a href="#">Gigante B10</a></li>
                                <li><a href="#">Dragão B10</a></li>
                                <li><a href="#">Necro B10</a></li>
                                <li><a href="#">Tower of Ascension - Normal</a></li>
                                <li><a href="#">Tower of Ascension - Hard</a></li>
                                <li><a href="#">Raid 5</a></li>
                                <li><a href="#">Siege Comps</a></li>
                            </ul>
                        </li>
                        <li class="{{ request()->is('contact') ? 'active' : '' }}"><a href="{{route('contact')}}">Contato</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <!-- Header section end -->

    <main class="content">
        @yield('content')
    </main>

    <!-- Footer section -->
    <footer class="footer-section">
        <div class="container">
            <div class="footer-left-pic">
                <img src="./images/footer-left-pic.png" alt="">
            </div>
            <div class="footer-right-pic">
                <img src="./images/footer-right-pic.png" alt="">
            </div>
            <a href="#" class="footer-logo">
                <img src="./images/logo.png" alt="">
            </a>
            <ul class="main-menu footer-menu">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="review.html">Sobre</a>
                </li>
                <li><a href="#">Tutorials</a>
                </li>
                <li><a href="review.html">Contato</a>
                </li>
            </ul>
            <div class="footer-social d-flex justify-content-center">
                <a href="#"><i class="fa fa-pinterest"></i></a>
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-instagram"></i></a>
                <a href="#"><i class="fa fa-youtube"></i></a>
            </div>
            <div class="copyright">Copyright &copy; <script>
                    document.write(new Date().getFullYear());
                </script> All rights reserved | | Design by <a href="#"> SmaxxioNVD </a>
            </div>
        </div>
    </footer>

    <!--====== Javascripts & Jquery ======-->
    <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.slicknav.min.js')}}"></script>
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/jquery.sticky-sidebar.min.js')}}"></script>
    <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
</body>

</html>