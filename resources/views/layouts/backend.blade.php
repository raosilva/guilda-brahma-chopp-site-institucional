<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Site de Recrutamento e Gerenciamento da Guilda Brahma Chopp - Summoners Wars">
    <meta name="keywords" content="summoners, wars, summoners wars, game, gaming, guild, guilda brahma chopp">

    <!-- CSRF Token -->
    <meta name=" csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Guilda Brahma Chopp') }} - @yield('title')</title>

    <!-- Favicon -->
    <link href="/public/images/favicon.ico" rel="shortcut icon" />

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="https://kit.fontawesome.com/e943c11967.js" crossorigin="anonymous"></script>

    <!-- Stylesheets -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slicknav.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/magnific-popup.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">

    <!-- Main Stylesheets -->
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

    <!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        var mini = true;

function toggleSidebar() {
    if (mini) {
        console.log("opening sidebar");
        document.getElementById("adminSidebar").style.width = "250px";
        document.getElementById("mainContent").style.marginLeft = "250px";
        this.mini = false;
    } else {
        console.log("closing sidebar");
        document.getElementById("adminSidebar").style.width = "85px";
        document.getElementById("mainContent").style.marginLeft = "85px";
        this.mini = true;
    }
}
    </script>
</head>

<body>

    <!-- Page Preloader -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header section -->
    <header class="header-section">
        <div class="header-warp">
            <div class="d-flex justify-content-end">
                <nav class="top-nav-area w-100">
                    <div class="user-panel">
                        @auth
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            Olá, {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                        @endauth
                    </div>
                </nav>
            </div>
        </div>
    </header>

    <div id="adminSidebar" class="adminSidebar" onmouseover="toggleSidebar()" onmouseout="toggleSidebar()">
        <a href="{{route('painel')}}" class="{{ (strpos(Route::currentRouteName(), 'painel') === 0) ? 'active' : '' }}"><span><i class="material-icons">info</i><span class="icon-text">dashboard</span></a><br>
        <a href="#"><i class="material-icons">group</i><span class="icon-text"></span>members</a></span>
        </a><br>
        <a href="#"><i class="material-icons">notifications_active</i><span class="icon-text"></span>guild
            wars</span></a><br>
        <a href="#"><i class="material-icons">emoji_events</i><span class="icon-text"></span>siege battle</span></a><br>
        <a href="#"><i class="material-icons">landscape</i><span class="icon-text"></span>labirinto</span></a><br>
        <a href="{{route('message-listing')}}"
            class="{{ (strpos(Route::currentRouteName(), 'message-listing') === 0) ? 'active' : '' }}"><i
                class="material-icons">email</i><span class="icon-text"></span>suporte<span></a>
    </div>

    @yield('mainArea')

    <!-- Footer section -->
    <footer class="fixed-bottom text-center p-3" style="background-color: #081624">
        <div class="copyright">Copyright &copy; <script>
                document.write(new Date().getFullYear());
            </script> All rights reserved | | Design by <a href="#"> SmaxxioNVD </a>
        </div>
    </footer>

    <!--====== Javascripts & Jquery ======-->
    <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.slicknav.min.js')}}"></script>
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/jquery.sticky-sidebar.min.js')}}"></script>
    <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    {{-- <script src="{{asset('js/scripts.js')}}" defer></script> --}}

</body>

</html>