<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Lyric;
use Illuminate\Http\Request;

class PageController extends Controller
{
    // public function index()
    // {
    //     return view('site.home');
    // }

    public function about()
    {
        return view('site.about');
    }

    public function lyrics($letter)
    {
        if (empty($letter)) {
            $lyrics = Lyric::orderBy('title', 'ASC')->paginate('30');

            return view('site.lyrics', [
                'lyrics' => $lyrics
            ]);
        } else {

            $lyrics = Lyric::where('title', 'like', $letter . '%')->orderBy('title', 'ASC')->paginate('30');

            return view('site.lyrics', [
                'lyrics' => $lyrics
            ]);
        }
    }

    public function showlyrics($id)
    {
        $lyric = Lyric::find($id);

        return view('site.lyrics-view', [
            'lyric' => $lyric
        ]);
    }

    public function contact()
    {
        return view('site.contact');
    }
}
