<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    public function index()
    {
        return view('site.home');
    }

    public function swchronicles()
    {
        return view('site.swchronicles');
    }

    public function about()
    {
        return view('site.about');
    }

    public function guild()
    {
        return view('site.about-the-guild');
    }

    public function game()
    {
        return view('site.about-the-game');
    }

    public function recruitment()
    {
        return view('site.recruitment');
    }
}
