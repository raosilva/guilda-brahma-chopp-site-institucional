<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Member;

class MemberController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Member::orderBy('nome', 'ASC')->paginate(15);

        $loggedId = intval(Auth::id());

        return view('admin.members.index', [
            'members' => $members,
            'loggedId' => $loggedId
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.members.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only([
            'nome',
            'conta_principal',
            'conta_alt',
            'in_gvg',
            'in_siege'
        ]);

        $validator = Validator::make($data, [
            'nome' => ['required', 'string', 'max:100'],
            'conta_principal' => ['required', 'string', 'max:100', 'unique:members'],
            'conta_alt' => ['string', 'max:100', 'unique:members'],
            'in_gvg' => ['required', 'boolean'],
            'in_siege' => ['required', 'boolean']
        ]);

        if ($validator->fails()){
            return redirect()->route('admin.members.create')
            ->withErrors($validator)
            ->withInput();
        }

        $member = new Member;
        $member->nome = $data['nome'];
        $member->conta_principal = $data['conta_principal'];
        $member->conta_alt = $data['conta_alt'];
        $member->in_gvg = $data['in_gvg'] == 'true' ? 1 : 0;
        $member->in_siege = $data['in_siege'] == 'true' ? 1 : 0;
        $member->save();

        return redirect()->route('admin.members.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
