<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ContatoEnviarRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Contato;
use App\NotificacaoInterface;

class ContatoController extends Controller
{
    public function index()
    {
        return view('site.contato');
    }

    public function list()
    {
        $contatos = Contato::orderBy('id', 'ASC')->paginate(10);

        return view('admin.contact.lista', [
            'contatos' => $contatos
        ]);
    }

    public function send(Request $request, Contato $contato, NotificacaoInterface $notificar)
    {
        $contato = new Contato();
 
		$contato->nome = $request->get('nome');
		$contato->email = $request->get('email');
        $contato->assunto = $request->get('assunto');
        $contato->mensagem = $request->get('mensagem');
        $contato->categoria_id = $request->get('categoria_id');
 
        $contato->save();
        

	    // Notificando..
	    $notificar->notificar();
 
		return redirect()->route('contact');     
    }
}
