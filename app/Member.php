<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    /*
     * Belongs to Labirinto
     */
    public function labirinto(){
        return $this->belongsTo('App\Labirinto','id');
    }

    /*
     * Belongs to Guild Wars
     */
    public function gvg(){
        return $this->belongsTo('App\Gvg','id');
    }

    /*
     * Belongs to Siege
     */
    public function siege(){
        return $this->belongsTo('App\Siege','id');
    }
}
