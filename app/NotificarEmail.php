<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificarEmail extends Model implements NotificacaoInterface
{
    public function notificar(){
    	/*
    	 * Um código muito legal que manda email
    	 */
         echo "Notificando por email..";
    }
}
