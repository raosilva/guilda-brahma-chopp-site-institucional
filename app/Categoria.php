<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
   /*
     * HasMany Contatos
     */
    public function contatos(){
        return $this->hasMany(Contato::class);
    }
}
